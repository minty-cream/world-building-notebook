import React, { Component } from 'react';
import TopBar from './components/TopBar';
import {
	BrowserRouter as Router,
	Route
} from 'react-router-dom';
import {observer} from 'mobx-react';

import MapPage from './views/MapPage';
import NotePage from './views/NotePage';
import AddTilePage from './views/MapPage/AddTilePage';
import HomePage from './views/HomePage';
import SettingsPage from './views/SettingsPage';
import SearchPage from './views/SearchPage';
import DataStore from './store';


class App extends Component {
	render() {
		return (
			<Router>
				<main style={{'color':'rgb(51,51,51)'}}>
					<article>
						<header>
							{DataStore.map_name.get().length > 0 ?
									<h1>{DataStore.map_name.get()}</h1>:
									<h1>Name of World</h1>
							}
							<nav>
								<Route path="/" component={TopBar}/>
							</nav>
						</header>
						<Route exact path="/" component={HomePage} />
						<Route path="/note/:id" component={NotePage} />
						<Route exact path="/map/:id" component={MapPage} />
						<Route path="/map/:id/add_tile/:x/:y" component={AddTilePage} />
						<Route exact path="/map/:id/settings" component={SettingsPage} />
						<Route path="/search/:world_id/:terms" component={SearchPage} />
					</article>
				</main>
			</Router>
		);
	}
}

export default observer(App);
