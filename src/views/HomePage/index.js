import React, { Component } from 'react';
import {observer} from 'mobx-react';
import DataState from '../../store';
import LoginPage from '../LoginPage';
import WorldsPage from '../WorldsPage';

class HomePage extends Component {
	render() {
		if(DataState.loggedIn.get()){
			return (<WorldsPage></WorldsPage>);
		}else{
			return (<LoginPage></LoginPage>);
		}
	}
}

export default observer(HomePage);
