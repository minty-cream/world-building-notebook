import React, { Component } from 'react';
import request from 'superagent';
import DataState from '../../store';
import config from '../../config';

class Login extends Component {
	constructor(props){
		super(props);
		this.login=this.login.bind(this);
		this.register=this.register.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleCheck=this.handleCheck.bind(this);
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleUserCredentials=this.handleUserCredentials.bind(this);
		this.state={ 'remember_me':false };
		console.log("DataState",DataState);
	}

	handleUserCredentials(err, res){
		this.setState({"error":false});
		console.log("res",res);
		if(res && res.status >= 400){
			this.setState({"error":res.body.message});
		}
		if(res && res.status === 200){
			DataState.login(res.body,this.state.remember_me);
		}
	}

	login(){
		request
			.post(config.api_url+"/users/login")
			.send({
				"username":this.state.email,
				"password":this.state.password
			})
			.end(this.handleUserCredentials);
	}

	register(){
		request
			.post(config.api_url+"/users")
			.send({
				"username":this.state.email,
				"password":this.state.password
			})
			.end(this.login);
	}

	handleChange(event){
		const target=event.target;
		const name=target.name;
		const value=target.value;
		this.setState({[name]:value});
	}

	handleCheck(){
		this.setState((state)=>({remember_me:!state.remember_me}));
	}

	handleSubmit(event){
		event.preventDefault();
	}

	render() {
		return (
			<section>
				<header><h2>Login or Register</h2></header>
				<p>
					Hi, this is a notebook made to help you build and document worlds. It has a map, a notebook, and a relationship tree.
				</p>
				<p>
					Feel free to email <a href="mailto:antoniomenteguiaga@gmail.com">Antonio Menteguiaga</a> with any questions, concerns, or suggestions.
				</p>
				{this.state.error ?  <p> Error: {this.state.error} </p> :<span></span>}
				<form style={{'display':'flex','flexDirection':'column'}} onSubmit={this.handleSubmit}>
					<label htmlFor="email">Email</label>
					<input type="email" name="email" id="password" onChange={this.handleChange}/>
					<label htmlFor="password">Password</label>
					<input type="password" name="password" id="password" onChange={this.handleChange}/>
					<div style={{'display':'flex','justifyContent':'space-between'}}>
						<label htmlFor="remember_me" style={{'flex':'1', 'cursor':'pointer'}}>Remember Me</label>
						<input type="checkbox" name="remember_me" onChange={this.handleCheck} id="remember_me"/>
					</div>
					<button onClick={this.login}>Login</button>
					<button onClick={this.register}>Register</button>
				</form>
			</section>
		);
	}
}

export default Login;
