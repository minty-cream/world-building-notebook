import React, { Component } from 'react';
import {observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import DataStore from '../../store';
import ImageBox from '../../components/ImageBox';
import request from 'superagent';
import config from '../../config';

class SettingsPage extends Component {
	constructor(props){
		super(props);
		this.state={
			name:'',
			cover:'',
			confirm_delete:false
		};
		this.logout=this.logout.bind(this);
		this.handleUpload=this.handleUpload.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleCheck=this.handleCheck.bind(this);
		this.handleCover=this.handleCover.bind(this);
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
		console.log("this",this);
	}

	logout(){
		DataStore.logout();
		console.log("this",this);
		this.props.history.push("/");
	}

	handleSubmit(event){
		event.preventDefault();
		console.log("Butts");
		request
			.put(`${config.api_url}/worlds/${this.props.match.params.id}`)
			.withCredentials()
			.send({
				name:this.state.name
			})
			.end((err,res)=>{
				this.props.history.push("/");
			});
	}

	handleCover(event){
		event.preventDefault();
		request
			.put(`${config.api_url}/worlds/${this.props.match.params.id}`)
			.withCredentials()
			.send({
				cover:this.state.cover
			})
			.end((err,res)=>{
				this.props.history.push("/");
			});
	}

	handleDelete(event){
		event.preventDefault();
		if(this.state.confirm_delete){
			console.log("Delete conform");
			request
				.del(`${config.api_url}/worlds/${this.props.match.params.id}`)
				.withCredentials()
				.end((err,res)=>{
					this.props.history.push("/");
				});
		}
	}

	handleUpload(event){
		let reader=new FileReader();
		let file=event.target.files[0];
		reader.onload=(upload)=>{
			this.setState({cover:upload.target.result});
		};

		if(file){
			reader.readAsDataURL(file);
		}
	}

	handleChange(event){
		const target=event.target;
		const name=target.name;
		const value=target.value;
		this.setState({[name]:value});
	}

	handleCheck(event){
		const target=event.target;
		const name=target.name;
		this.setState((state)=>{
			return ({[name]:!state[name]});
		}); 
	}                     


	render() {
		let _style={
			'lineHeight':'3rem',
			'borderTop':'3px solid black',
			'borderBottom':'3px solid black',
			'fontSize':'1.5rem',
			'marginTop':'1rem',
			'cursor':'pointer'
		};

		let _linkStyle={
			'display':'block'
		};

		let _fieldset={
			'display':'flex',
			'flexDirection':'column'
		}

		return (
			<section>
				<menu style={{'listStyle':'none','lineHeight':'1.5rem','padding':'0','margin':'0'}}>
					<li style={_style}><span style={_linkStyle} onClick={this.logout}>Log Out</span></li>
					<li style={_style}><Link style={_linkStyle} to="/">Change World</Link></li>
				</menu>
				<form onSubmit={this.handleCover}>
					<fieldset style={_fieldset}>
						<legend>Change World Cover Image</legend>
						<ImageBox src={this.state.cover}></ImageBox>
						<input type="file" id="cover" name="cover" onChange={this.handleUpload}/>
						<button>Save</button>
					</fieldset>
				</form>
				<form onSubmit={this.handleSubmit}>
					<fieldset style={_fieldset}>
						<legend>Rename World</legend>
						<label htmlFor="name">Name</label>
						<input type="text" id="name" name="name" placeholder="Change name..." value={this.state.name} onChange={this.handleChange}/>
						<button>Save</button>
					</fieldset>
				</form>
				<form onSubmit={this.handleDelete}>
					<fieldset style={_fieldset}>
						<legend>Delete World</legend>
						<div style={{'display':'flex','justifyContent':'space-between'}}> 
							<label htmlFor="confirm_delete" style={{'flex':'1', 'cursor':'pointer'}}>Confirm Desire to Delete</label>
							<input type="checkbox" name="confirm_delete" onChange={this.handleCheck} id="confirm_delete"/>
						</div>
						<button>Delete</button>
					</fieldset>
				</form>
			</section>
		);
	}
}

export default observer(SettingsPage);
