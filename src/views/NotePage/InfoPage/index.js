import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import config from '../../../config';
import ImageBox from '../../../components/ImageBox';
import request from 'superagent';

class NoteInfoPage extends Component {
	constructor(props){
		super(props);
		this.state={
			name:'',
			cover:'',
			description:'',
			tags:''
		};
	}

	componentDidMount(){
		request
			.get(`${config.api_url}/notes/${this.props.match.params.id}`)
			.withCredentials()
			.end((err,res)=>{
				console.log("err",err);
				console.log("res",res);
				if(res && res.status === 200){
					let _tags='';
					if(res.body.tags){
						_tags=res.body.tags.reduce((acc,curr)=>`${acc}${curr}, `,'');
						_tags=_tags.slice(0,-2);
					}
					this.setState({
						name:res.body.name,
						cover:res.body.cover,
						description:res.body.description,
						tags:_tags
					});
				}
			});
	}

	render() {
		return (
			<form style={{'display':'flex','flexDirection':'column'}} onSubmit={this.handleSubmit}>
				<ImageBox src={this.state.cover}></ImageBox>
				<section>
					<header>Name</header>
					<p>{this.state.name}</p>
				</section>
				<section>
					<header>Details</header>
					<p>{this.state.description}</p>
				</section>
				<section>
					<header>Tags</header>
					<p>{this.state.tags}</p>
				</section>
				<Link to={`/note/${this.props.match.params.id}/edit`}>Edit</Link>
			</form>
		);
	}
}

export default NoteInfoPage;
