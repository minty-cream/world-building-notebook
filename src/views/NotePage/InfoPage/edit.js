import React, { Component } from 'react';
import config from '../../../config';
import store from '../../../store';
import ImageBox from '../../../components/ImageBox';
import request from 'superagent';

class EditNoteInfoPage extends Component {
	constructor(props){
		super(props);
		this.state={
			name:'',
			description:'',
			tags:'',
			cover:''
		};
		this.handleChange=this.handleChange.bind(this);
		this.handleCheck=this.handleCheck.bind(this);
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleUpload=this.handleUpload.bind(this);
	}

	componentDidMount(){
		request
			.get(`${config.api_url}/notes/${this.props.match.params.id}`)
			.withCredentials()
			.end((err,res)=>{
				console.log("err",err);
				console.log("res",res);
				if(res && res.status === 200){
					let _tags='';
					if(res.body.tags){
						_tags=res.body.tags.reduce((acc,curr)=>`${acc}${curr},`,'');
					}
					this.setState({
						name:res.body.name,
						cover:res.body.cover,
						description:res.body.description,
						tags:_tags
					});
				}
			});
	}

	handleChange(event){
		const target=event.target;
		const name=target.name;
		const value=target.value;
		this.setState({[name]:value});
	}

	handleUpload(event){
		let reader=new FileReader();
		let file=event.target.files[0];
		reader.onload=(upload)=>{
			this.setState({cover:upload.target.result});
		};

		if(file){
			reader.readAsDataURL(file);
		}
	}

	handleSubmit(event){
		event.preventDefault();
		let _tags=this.state.tags.split(',').filter((curr)=>curr.trim()!=='').map((curr)=>curr.trim());
		let _info={
			name:this.state.name,
			description:this.state.description,
			cover:this.state.cover,
			tags:_tags,
		}
		if(this.state.archived){
			_info.metatags={
				map:store.map_id.get(),
				archived:this.state.archived
			}
		}
		request
			.put(`${config.api_url}/notes/${this.props.match.params.id}`)
			.withCredentials()
			.send(_info)
			.end((err,res)=>{
				if(res && res.status === 200){
					this.props.history.push(`/note/${this.props.match.params.id}`);
				}
			});

	}

	handleCheck(event){
		const target=event.target;
		const name=target.name;
		this.setState((state)=>{
			return ({[name]:!state[name]});
		}); 
	}                     

	render() {
		return (
			<form style={{'display':'flex','flexDirection':'column'}} onSubmit={this.handleSubmit}>
				<ImageBox src={this.state.cover}></ImageBox>
				<input type="file" id="cover" name="cover" onChange={this.handleUpload}/>
				<label htmlFor="name">Name</label>
				<input type="text" id="name" name="name" placeholder="Change name..." value={this.state.name} onChange={this.handleChange}/>
				<label htmlFor="details">Details</label>
				<textarea id="description" name="description" placeholder="Edit the description..." value={this.state.description} onChange={this.handleChange}/>
				<label htmlFor="tags">Tags</label>
				<input type="text" id="tags" name="tags" placeholder="Add, some, tags" value={this.state.tags} onChange={this.handleChange}/>
				<div style={{'display':'flex','justifyContent':'space-between'}}> 
					<label htmlFor="archived" style={{'flex':'1', 'cursor':'pointer'}}>Archived</label>
					<input type="checkbox" name="archived" onChange={this.handleCheck} id="archived"/>
				</div>
				<button>Save</button>
			</form>
		);
	}
}

export default EditNoteInfoPage;
