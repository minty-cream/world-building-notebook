import React, { Component } from 'react';
import helper from '../../../helper';
import store from '../../../store';
import SelectNotePage from './SelectNotePage';

class AddRelationshipPage extends Component {
	constructor(props){
		super(props);
		this.state={
			searchTerm:'',
			name:''
		};
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.pageSelected=this.pageSelected.bind(this);
	}

	handleSubmit(event){
		event.preventDefault();
		this.setState(currentState => {
			return {searchTerm: currentState.name}
		},()=>{
			helper.fillResults(store.map_id.get(),this.state.searchTerm,this);
		});
	}

	handleChange(event){
		const target=event.target;
		const name=target.name;
		const value=target.value;
		this.setState({[name]:value});
	}

	pageSelected(err,res){
		console.log("err",err);
		console.log("res",res);
		if(res && res.status ===200){
			console.log("GOIN");
			this.props.history.push(`/note/${this.props.match.params.id}/add_relationship/${res.body.id}`);
		}
	}

	render() {
		return(
			<section>
				<form style={{'display':'flex','flexDirection':'column'}} onSubmit={this.handleSubmit}>
					<h3>Select Relationship</h3>
					<label htmlFor="name">Who is this card related to?</label>
					<input type="text" id="name" name="name" placeholder="Write their name in this box." onChange={this.handleChange}/>
					<button>Search</button>
				</form>
				<SelectNotePage results={this.state.results} searchTerm={this.state.searchTerm} pageSelected={this.pageSelected}></SelectNotePage>
			</section>
		);
	}
}

export default AddRelationshipPage;
