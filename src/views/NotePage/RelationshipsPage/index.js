import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import InfoBox from '../../../components/InfoBox';
import request from 'superagent';
import config from '../../../config';

class NoteRelationshipsPage extends Component {
	constructor(props){
		super(props);
		this.state={
			relationships:[]
		}
		this.deleteRelationship=this.deleteRelationship.bind(this);
	}

	componentDidMount(){
		request
			.get(`${config.api_url}/relationships/`)
			.query(`{"rootid":"${this.props.match.params.id}"}`)
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status===200){
					this.setState({relationships:res.body});
				}
			});
	}

	deleteRelationship(_id){
		request
			.del(`${config.api_url}/relationships/${_id}`)
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status===200){
					this.setState((state)=>{
						return {
							relationships: state.relationships.filter((curr)=>curr.id!==_id)
						}
					});
				}
			});
	}

	render() {
		return (
			<section>
				<Link to={`/note/${this.props.match.params.id}/add_relationship`}>
					Add Relationship
				</Link>
				<ul style={{'display':'flex','flex':'row','flexWrap':'wrap','listStyle':'none'}}>
					{this.state.relationships.map((curr)=>{
						return (
							<li><InfoBox src={curr.note.cover} name={curr.note.name} to={`/note/${curr.note.id}`}>
									<p style={{'borderBottom':'0.1rem solid black'}}>{curr.note.description}</p>
									<p>{curr.properties.relationship}</p>
									<button onClick={()=>{this.deleteRelationship(curr.id)}}>Delete Relationship</button>
							</InfoBox></li>);
					})}
				</ul>
			</section>
		);
	}
}

export default NoteRelationshipsPage;
