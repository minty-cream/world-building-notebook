import React, { Component } from 'react';
import config from '../../../config';
import InfoBox from '../../../components/InfoBox';
import request from 'superagent';

class AddRelationshipPage extends Component {
	constructor(props){
		super(props);
		this.state={
			name:'',
			description:'',
			cover:'',
			relation:'',
			mutual:false,
			inverted:false
		};
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleCheck=this.handleCheck.bind(this);
	}

	componentDidMount(){
		request.get(`${config.api_url}/notes/${this.props.match.params.node_id}`)
			.withCredentials()
			.end((err,res)=>{
				console.log("err",err);
				console.log("res",res);
				if(res && res.status === 200){
					this.setState({
						name:res.body.name,
						description:res.body.description,
						cover:res.body.cover
					})
				}
			});
	}

	handleSubmit(event){
		event.preventDefault();
		console.log(this.state);
		let _relationship={
			rootid:this.props.match.params.root_id,
			nodeid:this.props.match.params.node_id,
			properties:{'relationship':this.state.relation}
		};
		let _inverted_relationship={
			rootid:this.props.match.params.node_id,
			nodeid:this.props.match.params.root_id,
			properties:{'relationship':this.state.relation}
		};
		let _send_json=this.state.inverted?_inverted_relationship:_relationship;
		let _mutual_json=this.state.inverted?_relationship:_inverted_relationship;
		console.log("_send_json",_send_json);
		console.log("_mutual_json",_mutual_json);
		request
			.post(`${config.api_url}/relationships/`)
			.send(_send_json)
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status === 200){
					if(!this.state.mutual){
						this.props.history.push(`/note/${this.props.match.params.root_id}/relationships`);
						return;
					}
					request
						.post(`${config.api_url}/relationships/`)
						.send(_mutual_json)
						.withCredentials()
						.end((err,res)=>{
							if(res && res.status === 200){
								this.props.history.push(`/note/${this.props.match.params.root_id}/relationships`);
							}
						});
				}
			});
	}

	handleCheck(event){
		const target=event.target;
		const name=target.name;
		this.setState((state)=>{
			return ({[name]:!state[name]});
		}); 
	}                     

	handleChange(event){
		const target=event.target;
		const name=target.name;
		const value=target.value;
		this.setState({[name]:value});
	}

	render() {
		return (
			<section>
				<form style={{'display':'flex','flexDirection':'column'}} onSubmit={this.handleSubmit}>
					<h3>Add Relationship</h3>
					<InfoBox src={this.state.cover} name={this.state.name}>
						{this.state.description}
					</InfoBox>
					<label htmlFor="description">How is this card related?</label>
					<input type="text" id="relation" name="relation" placeholder="Answer that question with a meaningful description here, please." onChange={this.handleChange} value={this.state.relation}/>
					<div style={{'display':'flex','justifyContent':'space-between'}}> 
						<label htmlFor="mutual" style={{'flex':'1', 'cursor':'pointer'}}>Mutual</label>
						<input type="checkbox" name="mutual" onChange={this.handleCheck} id="mutual"/>
					</div>
					<div style={{'display':'flex','justifyContent':'space-between'}}> 
						<label htmlFor="inverted" style={{'flex':'1', 'cursor':'pointer'}}>Inverted</label>
						<input type="checkbox" name="inverted" onChange={this.handleCheck} id="inverted"/>
					</div>
					<button>Add Relationship</button>
				</form>
			</section>
		);
	}
}

export default AddRelationshipPage;
