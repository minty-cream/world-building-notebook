import React, { Component } from 'react';
import config from '../../../config';
import request from 'superagent';
import InfoBox from '../../../components/InfoBox';
import ImageBox from '../../../components/ImageBox';

class NoteGalleryPage extends Component {
	constructor(props){
		super(props);
		this.handleUpload=this.handleUpload.bind(this);
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.deleteImage=this.deleteImage.bind(this);
		this.state={
			'image':'',
			'caption':'',
			'gallery':[]
		}
	}

	componentDidMount(){
		request
			.get(`${config.api_url}/images/`)
			.query(`{"noteid":"${this.props.match.params.id}"}`)
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status === 200){
					this.setState({
						gallery:res.body.reverse()
					})
				}
			});
	}

	handleUpload(event){
		let reader=new FileReader();
		let file=event.target.files[0];
		reader.onload=(upload)=>{
			this.setState({image:upload.target.result});
		};

		if(file){
			reader.readAsDataURL(file);
		}
	}

	handleChange(event){
		const target=event.target;
		const name=target.name;
		const value=target.value;
		this.setState({[name]:value});
	}

	handleSubmit(event){
		event.preventDefault();
		request
			.post(`${config.api_url}/images/`)
			.withCredentials()
			.send({
				'image':this.state.image,
				'caption':this.state.caption,
				'noteid':this.props.match.params.id
			})
			.end((err,res)=>{
				if(res && res.status === 200){
					this.setState((state)=>{
						return {
							'image':'',
							'caption':'',
							'gallery':[res.body].concat(state.gallery)
						}
					})
				}
			});

	}

	deleteImage(_image_id){
		request
			.del(`${config.api_url}/images/${_image_id}`)
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status === 200){
					this.setState((state)=>{
						return {
							gallery: state.gallery.filter((curr)=>curr.id!==_image_id)
						}
					});
				}
			});
	}


	render() {
		return (
			<section>
				<form style={{'display':'flex','flexDirection':'column'}} onSubmit={this.handleSubmit}>
					<h3>Add File</h3>
					<label htmlFor="image">Image</label>
					<div
						style={{
							'display':'flex',
							'justifyContent':'space-between',
							'flexWrap':'wrap'
						}}>
						<input type="file" id="image" name="image" onChange={this.handleUpload}/>
						<ImageBox src={this.state.image}></ImageBox>
					</div>
					<label htmlFor="description">Description</label>
					<input type="text" id="caption" name="caption" placeholder="A few simple words to help you remember why you uploaded this image" value={this.state.caption} onChange={this.handleChange}/>
					<button>Add File</button>
				</form>

				<section style={{'display':'flex','flexDirection':'row','flexWrap':'wrap'}}>
					{
						this.state.gallery.map((curr)=>{
							return (<InfoBox src={curr.image}>
								<a href={curr.image} target="_blank">View Image</a>
								<button onClick={()=>{this.deleteImage(curr.id)}}>
									Delete Image
								</button>
								<p>{curr.caption}</p>
							</InfoBox>);
						})
					}
				</section>
			</section>
		);
	}
}

export default NoteGalleryPage;
