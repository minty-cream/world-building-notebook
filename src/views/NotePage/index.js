import React, { Component } from 'react';
import {                                                                                                                                                                
	Route,                                                                                                                                                          
	Link                                                                                                                                                            
} from 'react-router-dom';
import InfoPage from './InfoPage';
import EditInfoPage from './InfoPage/edit';
import GalleryPage from './GalleryPage';
import RelationshipsPage   from './RelationshipsPage';
import SelectRelationshipPage from './RelationshipsPage/SelectRelationshipPage';
import AddRelationshipPage from './RelationshipsPage/AddRelationshipPage';
import request from 'superagent';
import config from '../../config';
import store from '../../store';


class NotePage extends Component {
	componentDidMount(){
		store.note_id.set(this.props.match.params.id);
		request
			.get(config.api_url+"/notes")
			.withCredentials()
			.query(`{"id":"${this.props.match.params.id}","hideCover":true}`)
			.end((err,res)=>{
				console.log("err",err);
				console.log("res",res);
				if(res && res.status === 200){
					if(res.body.metatags.map){
						store.changeMap(res.body.metatags.map);
					}
				}
			});
	}

	componentWillUnmount(){
		store.note_id.set('');
	}

	render() {
		return (
			<section>
				<menu style={{'display':'flex','flexDirection':'row','justifyContent':'space-around','listStyle':'none','padding':'0','margin':'0'}}>
					<li><Link to={`/note/${this.props.match.params.id}`}>Info</Link></li>
					<li><Link to={`/note/${this.props.match.params.id}/gallery`}>Gallery</Link></li>
					<li><Link to={`/note/${this.props.match.params.id}/relationships`}>Relationships</Link></li>
				</menu>
				<Route exact path="/note/:id" component={InfoPage} />
				<Route exact path="/note/:id/edit" component={EditInfoPage} />
				<Route path="/note/:id/gallery" component={GalleryPage} />
				<Route path="/note/:id/relationships" component={RelationshipsPage} />
				<Route exact path="/note/:id/add_relationship" component={SelectRelationshipPage} />
				<Route exact path="/note/:root_id/add_relationship/:node_id" component={AddRelationshipPage} />
			</section>
		);
	}
}

export default NotePage;
