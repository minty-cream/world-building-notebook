import React, { Component } from 'react';
import request from 'superagent';
import InfoBoxList from '../../components/InfoBoxList';
import ImageBox from '../../components/ImageBox';
import config from '../../config';
import store from '../../store';

class WorldsPage extends Component {
	constructor(props){
		super(props);
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleUpload=this.handleUpload.bind(this);
		this.state={worlds:[],name:'',description:'',cover:''};
	}

	componentDidMount(){
		request
			.get(config.api_url+"/worlds")
			.withCredentials()
			.end((err,res)=>{
				console.log("res",res);
				let _res = res.body.map((curr)=>{
					return {
						"title":curr.name,
						"content":curr.description,
						"src":curr.cover,
						"id":curr.id,
						"to":"/map/"+curr.id
					};
				});
				this.setState({worlds:_res});
			});
		store.changeMap('');
	}

	handleSubmit(event){
		event.preventDefault();
		request
			.post(config.api_url+"/worlds")
			.withCredentials()
			.send({
				"name":this.state.name,
				"description":this.state.description,
				"cover":this.state.cover
			})
			.end((err,res)=>{
				if(res && res.status===200){
					let _res = {
						"title":res.body.name,
						"content":res.body.description,
						"src":res.body.cover,
						"id":res.body.id,
						"to":"/map/"+res.body.id
					};
					let _worlds = this.state.worlds || [];
					_worlds.push(_res);
					this.setState({
						worlds:_worlds,
						name:'',
						description:'',
						cover:''
					});
				}
			});
	}

	handleChange(event){
		const target=event.target;
		const name=target.name;
		const value=target.value;
		this.setState({[name]:value});
	}

	handleUpload(event){
		let reader=new FileReader();
		let file=event.target.files[0];
		reader.onload=(upload)=>{
			this.setState({cover:upload.target.result});
		};

		if(file){
			reader.readAsDataURL(file);
		}
	}

	render() {
		return (
			<section>
				<form style={{'display':'flex','flexDirection':'column'}} onSubmit={this.handleSubmit}>
					<h3>Add World</h3>
					<label htmlFor="name">Name</label>
					<input type="text" id="name" name="name" placeholder="The name of your world" onChange={this.handleChange} value={this.state.name}/>
					<label htmlFor="description">Description</label>
					<input type="text" id="description" name="description" placeholder="A few simple words to describe this world" onChange={this.handleChange} value={this.state.description}/>
					<label htmlFor="cover">Cover</label>
					<div 
						style={{
							'display': 'flex',
							'flexWrap':'wrap',
							'justifyContent':'space-between'
						}}>
						<input type="file" id="cover" name="cover" onChange={this.handleUpload}/>
						<ImageBox src={this.state.cover}></ImageBox>
					</div>
					<button>Create World</button>
				</form>
				<InfoBoxList info={this.state.worlds} reverse="true"></InfoBoxList>
			</section>
		);
	}
}

export default WorldsPage;
