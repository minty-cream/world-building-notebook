import React, { Component } from 'react';
import SelectNotePage from './SelectNotePage';
import helper from '../../../helper';
import DataStore from '../../../store';

class AddTilePage extends Component {
	constructor(props){
		super(props);
		this.state={searchValue:'',searchTerm:'',error:''};
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handlePageSelected=this.handlePageSelected.bind(this);
	}

	componentDidMount(){
		DataStore.changeMap(this.props.match.params.id);
	}

	handleSubmit(event){
		event.preventDefault();
		this.setState(currentState => {
			return {searchTerm: currentState.searchValue}
		},()=>{
			helper.fillResults(this.props.match.params.id,this.state.searchTerm,this);
		});
	}

	handleChange(event){
		let _newState={
			searchValue:event.target.value
		};
		this.setState(_newState);
	}

	handlePageSelected(err,res){
		this.setState({error:""});
		if(res && res.status === 200){
			this.props.history.push("/map/"+this.props.match.params.id);
		}else{
			this.setState({error:res.body.message});
		}
		console.log("err",err);
		console.log("res",res);
		console.log("Hello from parent",this.props);
	}

	render() {
		console.log("this.state",this.state);
		return (
			<section>
				<form style={{'display':'flex','flexDirection':'column'}} onSubmit={this.handleSubmit}>
					<label htmlFor="tileSearch">Search for Existing Page, or Enter Title</label>
					<input id="tileSearch" name="tileSearch" type="text" value={this.state.searchValue} onChange={this.handleChange}/>
					<button>Search</button>
				</form>
				<p>{this.state.error}</p>
				<SelectNotePage
					results={this.state.results}
					searchTerm={this.state.searchTerm}
					pageSelected={this.handlePageSelected}
					map_id={this.props.match.params.id}
					x={this.props.match.params.x}
					y={this.props.match.params.y}>
				</SelectNotePage>
			</section>
		);
	}
}

export default AddTilePage;
