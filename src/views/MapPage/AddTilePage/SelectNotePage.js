import React, { Component } from 'react';
import {observer} from 'mobx-react';
import InfoBox  from '../../../components/InfoBox';
import config from '../../../config';
import request from 'superagent';

class SelectNotePage extends Component {
	constructor(props){
		super(props);
		this.state={};
		this.style={
			button:{
				'border':'0',
				'background':'none',
				'cursor':'pointer',
				'padding':'0',
				'outline':'none'
			}
		};
		this.createNote=this.createNote.bind(this);
		this.selectNote=this.selectNote.bind(this);
	}

	selectNote(_id){
		request
			.put(`${config.api_url}/notes/${_id}`)
			.send({
				"metatags":{
					"map": this.state.map_id,
					"x": this.state.x,
					"y": this.state.y
				}
			})
			.withCredentials()
			.end(this.props.pageSelected);
	}

	createNote(){
		request
			.post(config.api_url+"/notes")
			.send({
				"metatags":{
					"map": this.state.map_id,
					"x": this.state.x,
					"y": this.state.y
				},
				"name":this.state.searchTerm
			})
			.withCredentials()
			.end(this.props.pageSelected);
	}

	componentWillReceiveProps(props){
		if(props){
			if(props.searchTerm){
				this.setState({searchTerm:props.searchTerm});
			}
			if(props.results){
				this.setState({results:props.results});
			}
			if(props.map_id){
				this.setState({map_id:props.map_id});
			}
			if(props.x){
				this.setState({x:props.x});
			}
			if(props.y){
				this.setState({y:props.y});
			}
		}
	}

	render() {
		let _results=[];
		let _style={'display':'flex','flex':'row','flexWrap':'wrap','listStyle':'none'};
		if(this.state.results){
			if(this.state.results.exact && this.state.results.exact.length > 0){
				_results.push(
					<section>
						<header>Exact Name Matches</header>
						<ul style={_style}>
							{
								this.state.results.exact.map((curr)=>
									<li>
										<button onClick={()=>{this.selectNote(curr.id)}} style={this.style.button}>
											<InfoBox src={curr.cover} name={curr.name}>
												{curr.description}
											</InfoBox>
										</button>
									</li>
								)
							}
						</ul>
					</section>
				);
			}
			if(this.state.results.name && this.state.results.name.length > 0){
				_results.push(
					<section>
						<header>Name Contains Matches</header>
						<ul style={_style}>
							{
								this.state.results.name.map((curr)=>
									<li>
										<button onClick={()=>{this.selectNote(curr.id)}} style={this.style.button}>
											<InfoBox src={curr.cover} name={curr.name}>
												{curr.description}
											</InfoBox>
										</button>
									</li>
								)
							}
						</ul>
					</section>
				);
			}
			if(this.state.results.description && this.state.results.description.length > 0){
				_results.push(
					<section>
						<header>Description Contains Matches</header>
						<ul style={_style}>
							{
								this.state.results.description.map((curr)=>
									<li>
										<button onClick={()=>{this.selectNote(curr.id)}} style={this.style.button}>
											<InfoBox src={curr.cover} name={curr.name}>
												{curr.description}
											</InfoBox>
										</button>
									</li>
								)
							}
						</ul>
					</section>
				);
			}
			if(this.state.results.tag && this.state.results.tag.length > 0){
				_results.push(
					<section>
						<header>Tags Contains Matches</header>
						<ul style={_style}>
							{
								this.state.results.tag.map((curr)=>
									<li>
										<button onClick={()=>{this.selectNote(curr.id)}} style={this.style.button}>
											<InfoBox src={curr.cover} name={curr.name}>
												{curr.description}
											</InfoBox>
										</button>
									</li>
								)
							}
						</ul>
					</section>
				);
			}
		}
		if(_results.length > 0){
			return (
				<section>
					{_results}
				</section>
			);
		}else if(this.state.searchTerm){
			return <button onClick={()=>this.createNote()} style={this.style.button}>
				<InfoBox name={this.state.searchTerm}>Doesn't exist. Would you like to create a page for it? Click here.</InfoBox>
			</button>;
		}else{
			return null;
		}
	}
}

export default observer(SelectNotePage);
