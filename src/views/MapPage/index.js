import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import DataStore from '../../store';
import config from '../../config';
import ImageBox from '../../components/ImageBox';
import request from 'superagent';

class MapPage extends Component {
	constructor(props){
		super(props);
		this.state={
			tiles: false,
			loading:false,
			lowestX:0,
			lowestY:0,
			highestX:0,
			highestY:0
		};
		this._renderList=this._renderList.bind(this);
		this._renderRow=this._renderRow.bind(this);
		this._renderCol=this._renderCol.bind(this);
	}

	componentDidMount(){
		DataStore.changeMap(this.props.match.params.id);
		this.setState({loading:true});
		request
			.get(`${config.api_url}/notes`)
			.withCredentials()
			.query(`{"metatags.map":"${this.props.match.params.id}"}`)
			.end((err,res)=>{
				console.log("err",err);
				console.log("res",res);
				if(res && res.status===200 && res.body.length > 0){
					let _map={};
					let _tiles=res.body;
					let _lowestX=0;
					let _lowestY=0;
					let _highestX=0;
					let _highestY=0;
					for(let i=0;i<_tiles.length;i++){
						if(_tiles[i].metatags.x > _highestX) _highestX=parseInt(_tiles[i].metatags.x,10);
						if(_tiles[i].metatags.x < _lowestX) _lowestX=parseInt(_tiles[i].metatags.x,10);
						if(_tiles[i].metatags.y > _highestY) _highestY=parseInt(_tiles[i].metatags.y,10);
						if(_tiles[i].metatags.y < _lowestY) _lowestY=parseInt(_tiles[i].metatags.y,10);
						if(!_map[_tiles[i].metatags.x])_map[_tiles[i].metatags.x]={};
						_map[_tiles[i].metatags.x][_tiles[i].metatags.y]=_tiles[i];
					}
					this.setState({
						tiles:_map,
						lowestX:_lowestX-1,
						lowestY:_lowestY-1,
						highestX:_highestX+1,
						highestY:_highestY+1,
						loading:false});
				}else{
					this.setState({loading:false});
				}
			});
	}

	render() {
		if(this.state.loading){
			return (<section><p>Loading, please wait!</p></section>);
		}
		return (
			<section>
				<table style={{'borderCollapse':'collapse','tableLayout':'fixed','margin':'auto'}}>
					<tbody>
						{this._renderList()}
					</tbody>
				</table>
			</section>
		);
	}

	_renderList(){
		if(this.state.tiles){
			let _out=[];
			for(let _y=this.state.lowestY;_y<=this.state.highestY;_y++){
				_out.push(<tr key={`[${_y}]`}>{this._renderRow(_y)}</tr>);
			}
			return _out;
		}else{
			return (<tr key="[0]">
				<td><Link to={`/map/${this.props.match.params.id}/add_tile/0/0`}>Add First Tile</Link></td>
			</tr>)
		}
	}

	_renderRow(_y){
		let _out=[];
		for(let _x=this.state.lowestX;_x<=this.state.highestX;_x++){
			_out.push(this._renderCol(_x,_y));
		}
		return _out;
	}

	_renderCol(_x,_y){
		if(this.state.tiles[_x] && this.state.tiles[_x][_y]){
			if(this.state.tiles[_x][_y].cover){
				return(
					<td key={this.state.tiles[_x][_y].id} style={{'padding':'0'}}>
						<Link to={`/note/${this.state.tiles[_x][_y].id}`}>
							<ImageBox src={this.state.tiles[_x][_y].cover}></ImageBox>
						</Link>
					</td>
				);
			}else{
				return(
					<td key={this.state.tiles[_x][_y].id} style={{'padding':'0'}}>
						<Link to={`/note/${this.state.tiles[_x][_y].id}`}>{this.state.tiles[_x][_y].name}</Link>
					</td>
				);
			}
		}else{
			return (<td key={`[${_x},${_y}]`} style={{'textAlign':'center'}}>
				<Link to={`/map/${this.props.match.params.id}/add_tile/${_x}/${_y}`}>Add Tile ({_x},{_y})</Link>
			</td>);
		}
	}


}

export default MapPage;
