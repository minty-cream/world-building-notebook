import React, { Component } from 'react';
import InfoBox  from '../../components/InfoBox';
import store from '../../store';
import config from '../../config';
import helper from '../../helper';
import request from 'superagent';

class SearchPage extends Component {
	constructor(props){
		super(props);
		this.state={
			results:{}
		};
		this.style={
			button:{
				'border':'0',
				'background':'none',
				'cursor':'pointer',
				'padding':'0',
				'outline':'none'
			}
		};
		this.createNote=this.createNote.bind(this);
	}

	componentDidMount(){
		store.changeMap(this.props.match.params.world_id);
		helper.fillResults(this.props.match.params.world_id,this.props.match.params.terms,this);
	}

	componentWillReceiveProps(props){
		helper.fillResults(props.match.params.world_id,props.match.params.terms,this);
	}

	createNote(){
		request
			.post(config.api_url+"/notes")
			.send({
				"metatags":{
					"map":store.map_id.get()
				},
				"name":this.props.match.params.terms
			})
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status === 200){
					this.props.history.push(`/note/${res.body.id}`);
				}
			});
	}

	render() {
		let _results=[];
		let _style={'display':'flex','flex':'row','flexWrap':'wrap','listStyle':'none'};
		if(this.state.results.exact && this.state.results.exact.length > 0){
			_results.push(
				<section>
					<header>Exact Name Matches</header>
					<ul style={_style}>
						{
							this.state.results.exact.map((curr)=>
								<li>
									<InfoBox src={curr.cover} to={`/note/${curr.id}`} name={curr.name}>
										{curr.description}
									</InfoBox>
								</li>
								)
						}
					</ul>
				</section>
			);
		}
		if(this.state.results.name && this.state.results.name.length > 0){
			_results.push(
				<section>
					<header>Name Contains Matches</header>
					<ul style={_style}>
						{
							this.state.results.name.map((curr)=>
								<li>
									<InfoBox src={curr.cover} to={`/note/${curr.id}`} name={curr.name}>
										{curr.description}
									</InfoBox>
								</li>
								)
						}
					</ul>
				</section>
			);
		}
		if(this.state.results.description && this.state.results.description.length > 0){
			_results.push(
				<section>
					<header>Description Contains Matches</header>
					<ul style={_style}>
						{
							this.state.results.description.map((curr)=>
								<li>
									<InfoBox src={curr.cover} to={`/note/${curr.id}`} name={curr.name}>
										{curr.description}
									</InfoBox>
								</li>
								)
						}
					</ul>
				</section>
			);
		}
		if(this.state.results.description && this.state.results.tag.length > 0){
			_results.push(
				<section>
					<header>Tag Matches</header>
					<ul style={_style}>
						{
							this.state.results.tag.map((curr)=>
								<li>
									<InfoBox src={curr.cover} to={`/note/${curr.id}`} name={curr.name}>
										{curr.description}
									</InfoBox>
								</li>
								)
						}
					</ul>
				</section>
			);
		}
		if(_results.length > 0){
			return (
				<section>
					{_results}
				</section>
			);
		}else{
			return <button onClick={()=>this.createNote()} style={this.style.button}>
				<InfoBox name={this.props.match.params.terms}>Doesn't exist. Would you like to create a page for it? Click here.</InfoBox>
			</button>;
		}
	}
}

export default SearchPage;
