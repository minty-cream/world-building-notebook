import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import store from '../../store';
import {observer} from 'mobx-react';

class TopBar extends Component {
	constructor(props){
		super(props);
		this.state={_searchTerm:''};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event){
		this.setState({_searchTerm:event.target.value});
	}

	handleSubmit(event){
		event.preventDefault();
		this.props.history.push(`/search/${store.map_id.get()}/${this.state._searchTerm}`);
		this.setState({_searchTerm:''});
	}

	render() {
		let _settings=undefined;
		if(store.map_id.get().length>0){
			_settings=`/map/${store.map_id.get()}/settings`;
		}
		return (
			<menu style={{'display':'flex','flexDirection':'row','flexWrap':'wrap','justifyContent':'space-around','listStyle':'none','padding':'0','margin':'0'}}>
				<li>
					{
						store.map_id.get().length > 0 ?
							<Link to={"/map/".concat(store.map_id.get())}>Map</Link>:
							store.loggedIn.get() ?
							<Link to="/">Worlds</Link>:
							<span></span>
					}
				</li>
				<li>
					{
						store.map_id.get().length > 0 ?
							<form onSubmit={this.handleSubmit}>
								<input name="search" value={this.state._searchTerm} onChange={this.handleChange} id="search" type="text"/>
								<button>Search</button>
							</form>:
							<span></span>
					}
				</li>
				<li>
					{
						store.loggedIn.get() ? 
							_settings?
							<Link to={_settings}>Settings</Link>:
							<span></span>:
							<Link to="/">Login/Register</Link>
					}
				</li>
			</menu>
		);
	}

}

export default observer(TopBar);
