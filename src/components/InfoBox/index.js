import React, { Component } from 'react';
import ImageBox from '../ImageBox';
import {Link} from 'react-router-dom';

class InfoBox extends Component {
	render() {
		let _style={'display':'flex','flexDirection':'column','background':'#999999','borderRadius':'0.25rem','padding':'0.75rem','margin':'0.5rem'};
		if(this.props.to){
			_style.border="dashed 0.25rem #111111";
			return (
				<section style={_style}>
					<Link to={this.props.to}>
						<header>{this.props.name}</header>
						<ImageBox src={this.props.src}></ImageBox>
					</Link>
					<p>{this.props.children}</p>
				</section>
			);
		}
		return (
			<section style={_style}>
				<header>{this.props.name}</header>
				<ImageBox src={this.props.src}></ImageBox>
				<p>{this.props.children}</p>
			</section>
		);
	}
}

export default InfoBox;
