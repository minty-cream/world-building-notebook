import React, { Component } from 'react';
import InfoBox from '../InfoBox';

class InfoBoxList extends Component {

	constructor(props){
		super(props);
		this._renderList=this._renderList.bind(this);
		this.state={info:props.info};
	}
	componentWillReceiveProps(props){
		this.setState({info:props.info});
	}

	render() {
		return (
			<ul style={{'display':'flex','flexDirection':'row','flexWrap':'wrap','justifyContent':'space-around','listStyle':'none','padding':'0','margin':'0'}}>
				{this._renderList()}
			</ul>
		);
	}
	_renderList(){
		if(this.state.info && this.state.info.length > 0){
			let _info=this.state.info.map((x,i)=>(
				<li key={x.id} style={{'flex':'20rem', 'padding':'0.25rem'}}>
					<InfoBox src={x.src} name={x.title} to={x.to}>{x.content}</InfoBox>
				</li>
			));
			if(this.props.reverse){
				_info.reverse();
			}
			return _info;
		}else{
			return null;
		}
	}
}

export default InfoBoxList;
