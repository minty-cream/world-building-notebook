import React, { Component } from 'react';

class ImageBox extends Component {
	render() {
		if(!this.props.src){
			return <span></span>;
		}
		return (
			<div style={{
				'width':this.props.width?this.props.width:'200px',
				'height':this.props.height?this.props.height:'200px',
				'backgroundImage':`url('${this.props.src}')`,
				'backgroundRepeat':'none',
				'backgroundPosition':'center center',
				'backgroundSize':'cover',
				'alignSelf':'center',
				'display':'block'
			}}>
		</div>
		);
	}
}

export default ImageBox;
