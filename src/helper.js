import request from 'superagent';
import config from './config';
export default {
	fillResults:(_world_id,_terms, _caller)=>{
		console.log("reload");
		_caller.setState({
			results:{}
		});
		request
			.get(`${config.api_url}/notes/`)
			.query(JSON.stringify({
				"metatags.map": _world_id,
				name: _terms
			}))
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status===200){
					_caller.setState((state)=>{
						state.results.exact=res.body;
						return state;
					});
				}
			});
		request
			.get(`${config.api_url}/notes/`)
			.query(JSON.stringify({
				"metatags.map": _world_id,
				name: {
					$regex:_terms,
					$options:'i'
				}
			}))
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status===200){
					_caller.setState((state)=>{
						state.results.name=res.body;
						return state;
					});
				}
			});
		request
			.get(`${config.api_url}/notes/`)
			.query(JSON.stringify({
				"metatags.map": _world_id,
				description: {
					$regex:_terms,
					$options:'i'
				}
			}))
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status===200){
					_caller.setState((state)=>{
						state.results.description=res.body;
						return state;
					});
				}
			});
		request
			.get(`${config.api_url}/notes/`)
			.query(JSON.stringify({
				"metatags.map": _world_id,
				tags: _terms
			}))
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status===200){
					_caller.setState((state)=>{
						state.results.tag=res.body;
						return state;
					});
				}
			});
	}
}
