import {observable} from 'mobx';
import request from 'superagent';
import config from './config';

class DataState{
	map_id=observable("");
	note_id=observable("");
	map_name=observable("");
	user=observable([]);
	loggedIn=observable(false);

	constructor(){
		this.login=this.login.bind(this);
		this.logout=this.logout.bind(this);
		this.changeMap=this.changeMap.bind(this);
		if(localStorage.user){
			let _user=JSON.parse(localStorage.user);
			document.cookie="sid="+_user.id;
			request
				.get(config.api_url+"/users/me")
				.withCredentials()
				.end((err,res)=>{
					if(res.status===200){
						this.login(_user,true);
					}else{
						this.logout();
					}
				});
		}
	}

	changeMap(_id){
		if(_id === this.map_id.get()){
			return false;
		}
		if(_id === ''){
			this.map_id.set('');
			this.map_name.set('');
			return;
		}

		request
			.get(config.api_url+"/worlds")
			.query(`{"id":"${_id}"}`)
			.withCredentials()
			.end((err,res)=>{
				if(res && res.status===200){
					this.map_id.set(res.body.id);
					this.map_name.set(res.body.name);
				}
			});
	}

	login(user,remember_me){
		if(user){
			this.user.replace([user]);
			document.cookie="sid="+user.id;
		}
		if(remember_me){
			localStorage.user=JSON.stringify(user);
		}
		this.loggedIn.set(true);
	}

	logout(){
		this.user.clear();
		this.loggedIn.set(false);
		delete localStorage.user;
		delete document.cookie;
	}
}

const singleton = new DataState();
export default singleton;
